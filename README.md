# Tiki Manager

## Installation

The preferred way to install this extension is through composer.
To install Tiki Manager it is required to have [composer](https://getcomposer.org/download/) installed.

```
php composer.phar install
```

## Documentation

Documentation is at [Tiki Documentation - Manager](https://doc.tiki.org/Manager).

## Contributing

Thanks for your interest in contributing! Have a look through our [issues](https://gitlab.com/tikiwiki/tiki-manager/issues) section and submit a merge request.
